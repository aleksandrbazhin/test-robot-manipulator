import os
import time
import math
import struct
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines

TIME_DELTA = 0.02
CONNECTION_FILE_IN = "to_robot.bin"
CONNECTION_FILE_OUT = "from_robot.bin"

class Robot:
    LINK_1_LENGTH = 10.0
    LINK_2_LENGTH = 5.0
    LINK_3_LENGTH = 5.0
    MAX_JOINT_ACCELERATION = 0.1
    MAX_JOINT_VELOCITY = 2.5
    theta1 = 0.0
    theta2 = 0.0
    theta3 = 0.0
    target_theta1 = 0.0
    target_theta2 = 0.0
    target_theta3 = 0.0
    theta1_velocity = 0.0
    theta2_velocity = 0.0
    theta3_velocity = 0.0
    joint1_position = (0.0, 0.0, 0.0)
    joint2_position = (0.0, 0.0, 0.0)
    joint3_position = (0.0, 0.0, 0.0)

    # infinite acceleration no regulation - just for debug
    def step(self, dt):

        max_joint_turn = self.MAX_JOINT_VELOCITY * dt
        joint1_remain = self.target_theta1 - self.theta1
        joint2_remain = self.target_theta2 - self.theta2
        joint3_remain = self.target_theta3 - self.theta3

        if math.fabs(joint1_remain) > max_joint_turn :
            self.theta1 += math.copysign(max_joint_turn, joint1_remain)
        else:
            self.theta1 = self.target_theta1
        if math.fabs(joint2_remain) > max_joint_turn :
            self.theta2 += math.copysign(max_joint_turn, joint2_remain)
        else:
            self.theta2 = self.target_theta2
        if math.fabs(joint3_remain) > max_joint_turn :
            self.theta3 += math.copysign(max_joint_turn, joint3_remain)
        else:
            self.theta3 = self.target_theta3

        self.joint1_position, self.joint2_position, self.joint3_position = \
            self.get_joint_coordinates(self.theta1, self.theta2, self.theta3)


    # forward kinematics
    def get_joint_coordinates(self, theta_1, theta_2, theta_3):
        point1_x = self.LINK_1_LENGTH * math.cos(theta_1)
        point1_y = self.LINK_1_LENGTH * math.sin(theta_1)
        point1_z = 0

        l2xy = self.LINK_2_LENGTH * math.cos(theta_2)

        point2_x = point1_x + l2xy * math.cos(theta_1)
        point2_y = point1_y + l2xy * math.sin(theta_1)
        point2_z = point1_z + self.LINK_2_LENGTH * math.sin(theta_2)

        l3_xy_angle = math.pi / 2.0 - theta_3 + theta_2

        l3xy = self.LINK_3_LENGTH * math.sin(l3_xy_angle)

        point3_x = point2_x + l3xy * math.cos(theta_1)
        point3_y = point2_y + l3xy * math.sin(theta_1)
        point3_z = point2_z - self.LINK_3_LENGTH * math.cos(l3_xy_angle)

        point1 = (point1_x, point1_y, point1_z)
        point2 = (point2_x, point2_y, point2_z)
        point3 = (point3_x, point3_y, point3_z)
        return point1, point2, point3


if __name__ == "__main__":
    robot = Robot()

    f = open(CONNECTION_FILE_IN, "rb")
    previous_content = f.read()
    f.close()

    fig, ax1 = plt.subplots()
    ax1.set_xlabel('x')
    ax1.set_ylabel('z')
    ax1.set_xlim([0, 20])
    ax1.set_ylim([-10, 10])

    last_time = time.time()

    while True:
        if os.path.getsize(CONNECTION_FILE_IN) == 0:
            print("NO FILE READY")
            time.sleep(TIME_DELTA)
            continue
        f = open(CONNECTION_FILE_IN, "rb")
        content = f.read()
        f.close()

        if previous_content != content: # new command received
            robot.target_theta1, robot.target_theta2, robot.target_theta3 = struct.unpack('fff', content)
            previous_content = content

            print("NEW TARGET", robot.target_theta1, robot.target_theta2, robot.target_theta3)

        robot.step(float(time.time() - last_time))
        last_time = time.time()

        f = open(CONNECTION_FILE_OUT, "wb")
        f.write(struct.pack('fff', robot.theta1, robot.theta2, robot.theta3))
        f.close()

        print("angles: ", robot.theta1, robot.theta2, robot.theta3)
        print("position: ", robot.joint3_position[0], robot.joint3_position[1], robot.joint3_position[2])

        ax1.cla()
        ax1.set_xlim([0, 20])
        ax1.set_ylim([-10, 10])
        j0 = (0, 0)
        j1_xz = (robot.joint1_position[0], robot.joint1_position[2])
        j2_xz = (robot.joint2_position[0], robot.joint2_position[2])
        j3_xz = (robot.joint3_position[0], robot.joint3_position[2])
        ax1.plot(*zip(j0, j1_xz, j2_xz, j3_xz), marker='o')

        plt.pause(TIME_DELTA)
        # time.sleep(TIME_DELTA)
