#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include "robot.h"
#include "path_point.h"

// uncoment for debug simulation and xz visualisation
#define WITH_PYTHON_EMULATOR_WIN
// #define WITH_PYTHON_EMULATOR_UNIX


int main()
{

#ifdef WITH_PYTHON_EMULATOR_WIN
     std::system("start python robot_emulator.py");
     std::this_thread::sleep_for(std::chrono::milliseconds(2000)); // wait till emulator starts
#endif

#ifdef WITH_PYTHON_EMULATOR_UNIX
    std::system("python robot_emulator.py &");
    std::this_thread::sleep_for(std::chrono::milliseconds(2000)); // wait till emulator starts
#endif

    Robot robot;

    std::ifstream input_file("input.in");
    if (!input_file) {
       std::cout << "Error: cannot open a file" << std::endl;
       return -1;
    }

    std::string line;
    while (std::getline(input_file, line)) {
        std::istringstream line_stream(line);
        double x, y, z, t;
        line_stream >> x >> y >> z >> t;
        std::chrono::milliseconds time_in_ms{static_cast<int>(t * 1000)};
        robot.addPathPoint(x, y, z, time_in_ms);
    }
    input_file.close();

    robot.runPath();

    std::cout << "finished" << std::endl;
    return 0;
}
