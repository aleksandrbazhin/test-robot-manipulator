TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    robot.cpp \
    connection.cpp

HEADERS += \
    robot.h \
    connection.h \
    joints.h \
    point3d.h \
    path_point.h \
    path_joints.h

DISTFILES += \
    input.in \
    robot_emulator.py

copydata.commands = $(COPY_FILE) $$shell_path($$PWD\\input.in) $$shell_path($$OUT_PWD)
copyemulator.commands = $(COPY_FILE) $$shell_path($$PWD\\robot_emulator.py) $$shell_path($$OUT_PWD)
first.depends = $(first) copydata
first.depends += $(first) copyemulator
export(first.depends)
export(copydata.commands)
export(copyemulator.commands)
QMAKE_EXTRA_TARGETS += first copydata copyemulator
