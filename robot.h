#ifndef ROBOT_H
#define ROBOT_H

#include <vector>
#include <queue>
#include <chrono>
#include "connection.h"
#include "joints.h"
#include "point3d.h"
#include "path_point.h"
#include "path_joints.h"

class Robot
{
    Connection robot_connection;
    std::queue<PathPoint> path;
    std::deque<PathJoints> joints_history;
    Joints position = Joints(0.0, 0.0, 0.0);
    double max_acceleration_along_path;

    static const double CONTROL_FREQUENCY;
    static const std::chrono::milliseconds CONTROL_PERIOD;
    static const unsigned char LINKS_NUMBER;
    static const unsigned char JOINT_POSITION_HISTORY_LENGTH;
    static const double LINK_1_LENGTH,
                       LINK_2_LENGTH,
                       LINK_3_LENGTH;
    static const Joints JOINTS_LIMITS_MIN,
                        JOINTS_LIMITS_MAX;
    std::chrono::steady_clock::time_point time_start_point;

public:
    Robot();
    ~Robot();
    void addPathPoint(double, double, double, std::chrono::milliseconds);
    void runPath();

private:
    Joints solveInverse(const Point3d&);
    Point3d solveForward(const Joints&);

    Joints vectorToJoints(const std::vector<unsigned char> &byte_data);
    std::vector<unsigned char> jointsToVector(const Joints &joints);

    int updatePositionInfo(const std::chrono::steady_clock::time_point &now);
    int orderMovement(const Joints &next_joints);
};

#endif // ROBOT_H
