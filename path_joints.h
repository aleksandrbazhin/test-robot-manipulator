#ifndef PATH_JOINTS_H
#define PATH_JOINTS_H

#include"joints.h"
#include<chrono>

struct PathJoints
{
    Joints joints;
    std::chrono::steady_clock::time_point time;
    PathJoints(double theta1, double theta2, double theta3,
               std::chrono::steady_clock::time_point time_init)
        : joints(theta1, theta2, theta3), time(time_init){}

};

#endif // PATH_JOINTS_H
