#include "connection.h"
#include <iostream>
#include <fstream>
#include <string>

const unsigned int VECTOR_SIZE = 12;
const std::string TO_ROBOT_FILE("to_robot.bin"), FROM_ROBOT_FILE("from_robot.bin");

int Connection::open()
{
    // create file stubs
    std::ofstream outfile(TO_ROBOT_FILE), infile(FROM_ROBOT_FILE);;
    outfile.close();
    std::vector<unsigned char> test_data {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    infile.write(reinterpret_cast <const char*> (test_data.data()),
                  VECTOR_SIZE * sizeof(char));
    infile.close();
    return 1;
}

int Connection::close()
{
    return 1;
}

// why parameter is not const reference?
int Connection::send(std::vector<unsigned char> &data)
{
    std::ofstream outfile(TO_ROBOT_FILE, std::ofstream::binary | std::fstream::trunc);
    outfile.write(reinterpret_cast <const char*> (data.data()),
                  VECTOR_SIZE * sizeof(char));

    outfile.close();
    return 1;
}

int Connection::receive(std::vector<unsigned char> &data)
{
    std::ifstream infile(FROM_ROBOT_FILE, std::ifstream::binary | std::fstream::in | std::fstream::ate);
    int filesize = infile.tellg();
    const size_t count = filesize / sizeof(char);
    infile.seekg(0);
    if (count == VECTOR_SIZE) {
        char buffer[VECTOR_SIZE];
        infile.read(buffer, VECTOR_SIZE * sizeof(char));
        std::copy(&buffer[0], &buffer[VECTOR_SIZE], data.data());
        return 1; // data of correct size
    }
    return 0;
}
