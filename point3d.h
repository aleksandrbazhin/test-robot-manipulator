#ifndef POINT3D_H
#define POINT3D_H

#include <iostream>

struct Point3d
{
    double x;
    double y;
    double z;
    Point3d() : x(0.0), y(0.0), z(0.0) {}
    Point3d(double new_x, double new_y, double new_z)
        : x(new_x), y(new_y), z(new_z) {}
};


#endif // POINT3D_H
