#ifndef JOINTS_H
#define JOINTS_H

struct Joints
{
    double theta_1;
    double theta_2;
    double theta_3;
    Joints() : theta_1(0.0), theta_2(0.0), theta_3(0.0) {}
    Joints(double th1, double th2, double th3)
        : theta_1(th1), theta_2(th2), theta_3(th3) {}
};

#endif // JOINTS_H
