#ifndef PATH_POINT_H
#define PATH_POINT_H

#include "point3d.h"
#include <chrono>

// it consists of a 3d point + estimated time

struct PathPoint
{
    Point3d point;
    std::chrono::steady_clock::time_point time;
    PathPoint(double x, double y, double z,
              std::chrono::steady_clock::time_point time_init)
        : point(x, y, z), time(time_init){}
};


#endif // PATH_POINT_H
