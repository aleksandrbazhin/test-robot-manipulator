#include <iostream>
#include <algorithm>
#include <thread>
#include <chrono>
#include <ctime>
#include <math.h>
#include "robot.h"

const double Robot::CONTROL_FREQUENCY = 50.0;
const std::chrono::milliseconds Robot::CONTROL_PERIOD =
        std::chrono::milliseconds(static_cast<int>(1000 / Robot::CONTROL_FREQUENCY));
const unsigned char Robot::LINKS_NUMBER = 3;
const unsigned char Robot::JOINT_POSITION_HISTORY_LENGTH = 10;
const double Robot::LINK_1_LENGTH = 10.0;
const double Robot::LINK_2_LENGTH = 5.0;
const double Robot::LINK_3_LENGTH = 5.0;
const Joints Robot::JOINTS_LIMITS_MIN = Joints(-M_PI, -M_PI / 2.0, -M_PI);
const Joints Robot::JOINTS_LIMITS_MAX = Joints(M_PI, M_PI / 2.0, M_PI);


Robot::Robot()
{
    this->time_start_point = std::chrono::steady_clock::now();
    this->robot_connection.open();
}

Robot::~Robot()
{
    this->robot_connection.close();
}


void Robot::addPathPoint(double x, double y, double z, std::chrono::milliseconds time)
{
    this->path.push(PathPoint(x, y, z, this->time_start_point + time));
}


void Robot::runPath()
{
    while (!this->path.empty()) {
        PathPoint next_path_point(this->path.front());
        Joints target_joints = this->solveInverse(next_path_point.point);
        if(!this->orderMovement(target_joints)) {
            std::cout << "Error: can not send data to robot" << std::endl;
            return; // error in communication, exit main loop
        }

        auto now = std::chrono::steady_clock::now();
        std::chrono::duration<double> remains = next_path_point.time - now;
        std::chrono::duration<double> running_time = now - this->time_start_point;
        while (remains.count() > 0) {
            now = std::chrono::steady_clock::now();
            if (!this->updatePositionInfo(now)) {
                std::cout << "Error: no data from robot" << std::endl;
                return; // error in communication, exit main loop
            }
            remains = next_path_point.time - now;
            running_time = now - this->time_start_point;
            std::cout << "tick " << running_time.count() << std::endl;
            std::this_thread::sleep_for(CONTROL_PERIOD);
        }

        this->path.pop();
    }
}


int Robot::orderMovement(const Joints &next_joints)
{
    auto joints_data = this->jointsToVector(next_joints);
    if (!this->robot_connection.send(joints_data)) {
        return 0; //  error sending
    }
    return 1;
}


int Robot::updatePositionInfo(const std::chrono::steady_clock::time_point &now)
{
    std::vector<unsigned char> joints_vector_data(LINKS_NUMBER * sizeof(float));
    if (this->robot_connection.receive(joints_vector_data)) {
        this->position = vectorToJoints(joints_vector_data);

        // gather data on position history
        this->joints_history.push_back(
                    PathJoints(
                        this->position.theta_1,
                        this->position.theta_2,
                        this->position.theta_3,
                        now));
        if (this->joints_history.size() > JOINT_POSITION_HISTORY_LENGTH) {
            this->joints_history.pop_front();
        }
        return 1;
    } else {
        return 0; // error receiving
    }
}


// for such a simple robot there is an analytical solution
Joints Robot::solveInverse(const Point3d &point)
{
    Joints joints;
    joints.theta_1 = atan2(point.y, point.x);

    // length of links 2 and 3 projected on link 1
    double l23 = hypot(point.x, point.y) - LINK_1_LENGTH;

    // distance from the end of link 1 to the end of manipulator
    double d23 = hypot(l23, point.z);

    double l2sq = LINK_2_LENGTH * LINK_2_LENGTH,
           l3sq = LINK_3_LENGTH * LINK_3_LENGTH,
           d23sq = d23 * d23;

    double a1 = (l2sq + d23sq - l3sq) / (2.0 * LINK_2_LENGTH * d23);
    joints.theta_2 = atan2(point.z, l23) +
            acos( a1 );

    double a2 = (l2sq + l3sq - d23sq) / (2.0 * LINK_2_LENGTH * LINK_3_LENGTH);
    joints.theta_3 =
            M_PI - acos( a2 );

    // we do not check overall reachability since points are provided inside working area
    // however there are some points inside working area that are reachable
    // only from one pose due to theta2 limitations
    if (joints.theta_2 > JOINTS_LIMITS_MAX.theta_2 ||
            joints.theta_2 < JOINTS_LIMITS_MIN.theta_2) {
        joints.theta_2 -= 2.0 * acos(point.z / d23);
        joints.theta_3 = - joints.theta_3;
    }

    return joints;
}


Point3d Robot::solveForward(const Joints &joints)
{
    Point3d point1, point2, point3;
    point1.x = LINK_1_LENGTH * cos(joints.theta_1);
    point1.y = LINK_1_LENGTH * sin(joints.theta_1);
    point1.z = 0;

    // link's 2 projection length onto xy plane
    double l2xy = LINK_2_LENGTH * cos(joints.theta_2);

    point2.x = point1.x + l2xy * cos(joints.theta_1);
    point2.y = point1.y + l2xy * sin(joints.theta_1);
    point2.z = point1.z + LINK_2_LENGTH * sin(joints.theta_2);

    // link's 3 projection length onto xy plane
    double l3_xy_angle = M_PI / 2.0 - joints.theta_3 + joints.theta_2;
    double l3xy = LINK_3_LENGTH * sin(l3_xy_angle);

    point3.x = point2.x + l3xy * cos(joints.theta_1);
    point3.y = point2.y + l3xy * sin(joints.theta_1);
    point3.z = point2.z - LINK_3_LENGTH * cos(l3_xy_angle);

    return point3;
}


// convert from 12bit char vector to joint struct
// I guess little/big endian is always correct
Joints Robot::vectorToJoints(const std::vector<unsigned char> &byte_data)
{
    const float* f = reinterpret_cast <const float*> (byte_data.data());
    Joints joints;
    joints.theta_1 = static_cast<double> (f[0]);
    joints.theta_2 = static_cast<double> (f[1]);
    joints.theta_3 = static_cast<double> (f[2]);
    return joints;
}


// convert from joint struct to 12bit char vector
std::vector<unsigned char> Robot::jointsToVector(const Joints &joints)
{
    const float f[LINKS_NUMBER] = {
        static_cast<float> (joints.theta_1),
        static_cast<float> (joints.theta_2),
        static_cast<float> (joints.theta_3)};
    const unsigned char* uc = reinterpret_cast<const unsigned char*>(&f[0]);
    std::vector<unsigned char> data(uc, uc + LINKS_NUMBER * sizeof(float));
    return data;
}

